var dashboard = require('../controllers/dashboard_controller.js');
var admin = require('../controllers/admin_controller.js');
var Questions = require('../models/questions.js');
var User = require('../models/users.js');
var fs=require('fs');


function initializeRoutes(app, passport, cache){

	app.get('/dashboard',admin.isLoggedIn,function(req,res)
	{
		var all_ques;	
		Questions.find({},function(err,ques){
			if(err)
				console.log("Its an error");
			if(ques){
				all_ques=ques;
				
			fs.stat("./public/images/"+req.user._id+".jpg", function(err, stat) {
			    if (!err) {
			        res.render('dashboard.ejs',{def:"Home", user : req.user, questions : all_ques, image:"images/"+req.user._id+".jpg"});
			    }else {
			         res.render('dashboard.ejs',{def:"Home", user : req.user, questions : all_ques, image:"images/one.jpg"});
			    }
			});}
			else{
				console.log("Empty question database");
				res.render('dashboard.ejs',{user : req.user});
			}

		});
	});

	app.get('/ask',admin.isLoggedIn,function(req,res)
	{

		fs.stat("./public/images/"+req.user._id+".jpg", function(err, stat) {
			    if (!err) {
			       res.render('ask.ejs',{ def:"Ask Question",user : req.user,image:"images/"+req.user._id+".jpg"});
			    }else {
			       res.render('ask.ejs',{ def:"Ask Question", user : req.user, image:"images/one.jpg"});
			    }
			}); 
		
	})

	app.get('/profile',admin.isLoggedIn,function(req,res){
		fs.stat("./public/images/"+req.user._id+".jpg", function(err, stat) {
			    if (!err) {
			      res.render('profile.ejs',{def:"My Profile",user : req.user, image:"images/"+req.user._id+".jpg"})
			    }else {
			      res.render('profile.ejs',{def:"My Profile",user : req.user, image:"images/one.jpg"});
			    }
			});
	
	})

	app.get('/setting',admin.isLoggedIn,function(req,res){
		fs.stat("./public/images/"+req.user._id+".jpg", function(err, stat) {
			    if (!err) {
			      res.render('setting.ejs',{def:"My Account",user : req.user, image:"images/"+req.user._id+".jpg"})
			    }else {
			      res.render('setting.ejs',{def:"My Account",user : req.user, image:"images/one.jpg"});
			    }
			});
		
	})
		
	app.get('/answer',admin.isLoggedIn,function(req,res){
		fs.stat("./public/images/"+req.user._id+".jpg", function(err, stat) {
			    if (!err) {
			      res.render('answer.ejs',{def:"Question",user : req.user, image:"images/"+req.user._id+".jpg",id:dashboard.queryId(req,res)});
			    }else {
			      res.render('answer.ejs',{def:"Question",user : req.user, image:"images/one.jpg",id:dashboard.queryId(req,res)});
			    }
			});
		
	})
	//app.get('/video',dashboard.videocall);
	app.get('/mygroups',admin.isLoggedIn,function(req,res){
		fs.stat("./public/images/"+req.user._id+".jpg", function(err, stat) {
			    if (!err) {
			      res.render('mygroups.ejs',{def:"My Groups",user : req.user, image:"images/"+req.user._id+".jpg"});
			    }else {
			      res.render('mygroups.ejs',{def:"My Groups",user : req.user, image:"images/one.jpg"});
			    }
			});
		
	})


	app.get("/call",function(req,res){
		res.render("call.ejs");
	});

	app.get("/rating",function(req,res){
		res.render("rating.ejs");
	})




	app.get('/question',admin.isLoggedIn,dashboard.getQuestion);
	

	app.post("/settings",dashboard.updateChanges);
	app.post("/postQuestion",dashboard.postQuestion);

	app.post("/askedQuestion",dashboard.askedQuestion);
	app.post("/getQuestionById",dashboard.getQuestionById);

	app.post("/polling",dashboard.polling);
	app.post("/changePicture",dashboard.changePicture);

	app.post("/notifications",dashboard.notifications);
	app.post("/countNotify",dashboard.countNotify);

	app.post("/upvote",dashboard.upvote);
	app.post("/downvote",dashboard.downvote);

	app.post("/satisfied",dashboard.satisfied);
	app.post("/getPolledUsers",dashboard.getPolledUsers);

	app.post("/getUserById",dashboard.getUserById);
	app.post("/onlineUser",dashboard.onlineUser);

	app.post("/mygroupAsked",dashboard.mygroupAsked);
	app.post("/mygroupPolled",dashboard.mygroupPolled);	

	app.post("/alreadyPolled",dashboard.alreadyPolled);
	app.post("/updateCheck",dashboard.updateCheck);

	app.post("/satisfiedno",dashboard.satisfiedno);
	app.post("/satisfiedyes",dashboard.satisfiedyes);

}

module.exports = initializeRoutes;