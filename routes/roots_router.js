var roots = require('../controllers/roots_controller.js');
var mail  = require('../controllers/mail_controller.js');

function initializeRoutes(app, passport, cache){

	app.get('/',function(req,res){
		var flash = req.flash('error');
		var message = {
				message : flash
			};
	// Render the login page using the error messages from flash. 
			
		res.render('home.ejs',message);
	});
	
	app.get('/register', function(req,res){
		res.render('signup.ejs',{email : mail.decryptToken(req,res)});
	})
	/*app.post('/signup', passport.authenticate('local-signup', {
		successRedirect : '/signup',//redirect to the secure profile section
		failureRedirect : '/acc', // redirect back to the signup page if there is an error
		failureFlash : false // allow flash messages
	}));*/
	app.post('/register', roots.register);
}

module.exports = initializeRoutes;