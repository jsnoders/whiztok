$(document).ready(function(){
  $('#skilldiv').hide();
  if(!(window.location.search))
  {
      window.location.href="../";
  }
    
    $('#skilltxtbox').click(function(){
        $('#skilldiv').fadeIn("slow");
         $('#myULTags').focus();
    })
    
    $('#createUser').validate({
                 rules: {

                        fullName:{
                              required : true
                              },
                        pwd :{
                              required : true,
                              minlength: 6
                             },
                        cpwd:{
                             required: true,
                              equalTo: "#pwdtxtbox"
                              },
                        email:{
                              required: true
                        }
                       },
                       highlight: function (element) { // hightlight error inputs
                          $(element).css({"border-color": "#E2413E", 
                               "border-width":"1px", 
                                "border-style":"solid"
                                   });
                        },
                        unhighlight: function (element) { // revert the change done by hightlight
                          $(element).css({"border-color": "#4EBA6F", 
                                     "border-width":"1px", 
                                      "border-style":"solid"
                                    });
                        },
                        submitHandler: function (form) {
                          $.ajax({
                              url : $(form).attr('action'),
                              data : $(form).serialize(),
                              type : 'POST',
                              dataType: 'json', 
                              cache : false,
                              beforeSend : function() {
                                   
                              },
                              error : function(jqXHR, textStatus, errorThrown) {
                                var err=jqXHR.responseText;
                                console.log(err.error);
                                var innerHTML ='<div class="alert alert-danger">'+
                                jqXHR.responseText+
                                '</div>' ;               
                                $("#registerStatus").html(innerHTML);
                                console.log(jqXHR.error);
                                console.log(errorThrown);
                                },
                              success : function(data) {

                              if(data.error)
                              {
                                var innerHTML = '<div class="alert alert-danger">'+
                                data.error+
                                '</div>' ;               
                                $("#registerStatus").html(innerHTML);
                              }
                              if(data.value)
                              {

                                var innerHTML = '<div class="alert alert-success">'+
                                  '<script>setTimeout(function(){window.location.href="../"},2000);</script>'+
                                   'You are registered Sucessfully. Please login to continue.'+
                                   '</div>' ;               
                                $("#registerStatus").html(innerHTML); 
                              }  
                              return;                                  
                              },

                        complete : function() {}
                         });
                        }
  });
});                
                