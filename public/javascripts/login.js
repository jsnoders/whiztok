$(document).ready(function(){
  $('#loginform').validate({
                  ignore: "",
                  focusInvalid: false,
                  rules: { 
                      loginUsername: {
                          required: true,
                          email   : true,
                          
                      },
                      loginPassword:{
                          required :true,
                      },
                  },

                               
                  highlight: function (element) { // hightlight error inputs
                            $(element).css({"border-color": "#E2413E", 
                                 "border-width":"1px", 
                                  "border-style":"solid"
                            });
                  },

                  unhighlight: function (element) { // revert the change done by hightlight
                        $(element).css({"border-color": "#4EBA6F", 
                                 "border-width":"1px", 
                                  "border-style":"solid"});
                  },
                })
  $('#forgetform').validate({
                    focusInvalid: false,
                    rules: {
                        forgetemail:{
                          required: true,
                          email   : true, 
                        }
                    },           
                  highlight: function (element) { // hightlight error inputs
                            $(element).css({"border-color": "#E2413E", 
                                 "border-width":"1px", 
                                  "border-style":"solid"
                            });
                  },

                  unhighlight: function (element) { // revert the change done by hightlight
                        $(element).css({"border-color": "#4EBA6F", 
                                 "border-width":"1px", 
                                  "border-style":"solid"});
                  },
                  submitHandler: function (form) {
                          $.ajax({
                              url : '/forgotpwd',
                              data : $(form).serialize(),
                              type : 'POST',
                              dataType: 'json', 
                              cache : false,
                              beforeSend : function() {
                                   
                              },
                              error : function(jqXHR, textStatus, errorThrown) {
                                console.log(JSON.stringify(jqXHR));
                                var innerHtml='<div class="alert alert-danger col-md-9"> Error-'+
                                jqXHR.responseText+'</div>'
                                 $('#loginStatus').html(innerHtml);
                                },
                              success : function(data) {
                                if(data.data){
                                var innerHtml='<div class="alert alert-success col-md-9">'+
                                data.data+'</div><script>setTimeout(function(){window.location.href="../"},5000);</script>'
                                 $('#loginStatus').html(innerHtml);     
                                 }
                                 if(data.error)
                                 {
                                 var innerHtml='<div class="alert alert-danger col-md-9">'+
                                data.error+'</div><script>setTimeout(function(){window.location.href="../"},5000);</script>'
                                 $('#loginStatus').html(innerHtml);  
                                 }                     
                              },

                        complete : function() {}
                         });
                        }
  })
});
