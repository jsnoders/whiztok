$(document).ready(function(){
	var peercall,peerrec,call;
	var socket,loc,destinationid;
	var qid=window.location.search.substring(4)
	console.log(window.location.search.substring(4));
	ques_id=window.location.search.substring(4);
	loc=(window.location.protocol+'//'+window.location.host);
	socket=io.connect(loc);

	var id=$('#user_id').val();
	socket.emit('online',{userid:id});
if (window.existingCall) {
        window.existingCall.close();
      }
	socket.on('callrequest',function(data){
			var user_name=$('#user_name').val();
			var callresponse;
			$.ajax({
				url:'/getUserById',
				data:{'id':data.source},
				type:'POST',
				dataType: 'json',
				beforesend:function(){
					   	// console.log("before sending");
				},
			    error:function(err,a,b){
					   	// console.log("err occured");
				},
			    success:function(data){
					   	fullName=data[0].fullName,
					   	id=data[0]._id;
					   	callresponse=confirm(fullName+" is calling you for question you polled");
					   	if(callresponse==true){
					  	 		//$('#myModal').modal("show");
				 	 		alert("You have accepted the call.");
				  	 		var peerid=$('#peerid').val();
						  	socket.emit("callaccepted",{
						  		 	 	'source':id,
						  		 	 	'peerid':peerid,
						  		 	 	'name':user_name
						  	})
						}
						else{
						  	alert("You have rejected the call");
						  	socket.emit("callrejected",{
						  		 			'target':id,
						  		 			'name':user_name
						  	})
						}
				}
			})
	});

	socket.on('callrejected',function(data){
		alert("call rejectected by "+data.name);
	})

	socket.on('callaccepted',function(data){
		alert("Callaccepted by " +data.name);
		var callto=data.callto;			
		var peerid=$('#peerid').val();
		var peer = new Peer(peerid, {key: 'lwjd5qra8257b9'});
		peer.on('on',function(){
			console.log("peer created at caller side :-"+peer.id);
		});
		//console.log("peer created at caller side :-"+peer.id);
		navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
	          // Get audio/video stream
	    navigator.getUserMedia({audio: true, video: true}, function(stream){
	        // Set your video displays
	        $('#localvideo').prop('src', URL.createObjectURL(stream));
	        window.localStream = stream;
	        var call=peer.call(callto,stream);
	        call.on('stream', function(remoteStream) {
			      // Show stream in some video/canvas element
			 	$('#remotevideo').prop('src', URL.createObjectURL(remoteStream));
			  });
	        socket.emit("ready",
	        {
	        	'target':data.source,
	        	'peer':data.peer
	        });
	   	}, function(err)
		   	{ 
		   		console.log("falied to make a call"+err); 
		   	});
	});
	
	socket.on('ready',function(data){
		console.log("ready in client");
		var peerid=$('#peerid').val();
		 // console.log("peer here "+peerid)
		var peery = new Peer(peerid,{ key: 'lwjd5qra8257b9'});
			 // var peerrec = new Peer(peerid,{ key: 'lwjd5qra8257b9'});
		peery.on('on',function(){
			console.log("peer created at receiver side :-"+peer.id);
		});
		$('#myModalrec').modal("show");

		$('.receiver').val(data.source);
		destinationid=data.source;
		navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
		peery.on('call', function(call) {
			navigator.getUserMedia({audio: true, video: true}, function(stream){
	      		$('#localvideo').prop('src', URL.createObjectURL(stream));
	            window.localStream = stream;
	            call.answer(stream); // Answer the call with an A/V stream.
		   		call.on('stream', function(remoteStream) {
		   		$('#remotevideo').prop('src', URL.createObjectURL(remoteStream));
		   		});
	       });
		});
	});   
	
	$('#close').click(function(){
		$('#chatshow').val(" ");
              window.existingCall.close();
              
    });
	$('#upperClose').click(function(){
		$('#chatshow').val(" ");
		window.existingCall.close();
              
    });
	socket.on('message', function(msg) {
	    //console.log("Message received on receiver is"+JSON.stringify(msg));
	    var innerhtml=$("#chatshow").val()+msg.sendername+' : '+msg.message+'\n';
	    $("#chatshow").val(innerhtml);
	});

	var btn = document.getElementById('chattext');
	btn.onkeydown = function (e) {
	    e = e || window.event;
	    var keyCode = e.keyCode || e.which;
	    if(keyCode==13) {
	       	var user_name=$('#user_name').val();
			var destination=$('.receiver').val();
			var msg=$("#chattext").val();
			if(msg.length>0){
				var innerhtml=$("#chatshow").val()+' Me : '+msg+'\n';
			    $("#chatshow").val(innerhtml);
				socket.emit('message',
					{'message':msg ,
					'target':destination,
					'sendername':user_name});
				$("input#chattext").val("");
			}
		}
	}

	$("#myModal").delegate("#send","click",function(){
		var user_name=$('#user_name').val();
		var destination=$('.receiver').val();
		var msg=$("#chattext").val();
		if(msg.length>0){
			var innerhtml=$("#chatshow").val()+'Me : '+msg+'\n';
		    $("#chatshow").val(innerhtml);
			socket.emit('message',
				{'message':msg ,
				'target':destination,
				'sendername':user_name});
			$("input#chattext").val("");
			$("input#chattext").focus();
		}
	})
})
