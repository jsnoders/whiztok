module.exports = {
		development: {
			db: 'mongodb://localhost:27017/whiztok',
			app: {
				name: 'WhizTok'
			}
		},

		production: {
			db: process.env.MONGOLAB_URI || process.env.MONGOHQ_URL,
			app: {
				name: 'WhizTok'
			}
		}
}