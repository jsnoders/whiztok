/**
 * Initializes the Passport Local mechanism.
 */
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GitHubStrategy = require('passport-github').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var User = require('../models/users');
var login = require('../controllers/login_controller.js');
var configAuth = require('./Auth');
var moment=require('moment');

module.exports = function(passport, config) {

	passport.serializeUser(function(user, done) {
		console.log("Serialzing Userid: " + user.id);
		done(null, user.id);
	});

	passport.deserializeUser(function(id, done) {
		console.log("DeSerialzing : " + id);
		User.findOneAndUpdate(
			{_id : id},{last_activity:moment(),status:1}
		, function(err, user) {
			done(err, user);
		});
	});

	passport.use('local',new LocalStrategy({
		usernameField : 'loginUsername',
		passwordField : 'loginPassword'
	}, function(email,password, done) {
		console.log("Validating the " + email + ":" + password);
		login.isValidUserPassword(email, password, done);
	}));

	passport.use('facebook',new FacebookStrategy({

		// pull in our app id and secret from our auth.js file
        clientID        : configAuth.facebookAuth.clientID,
        clientSecret    : configAuth.facebookAuth.clientSecret,
        callbackURL     : configAuth.facebookAuth.callbackURL

    },

    // facebook will send back the token and profile
    function(token, refreshToken, profile, done) {

		// asynchronous
		process.nextTick(function() {
			
			// find the user in the database based on their facebook id
	        User.findOne({ 'facebook.id' : profile.id }, function(err, user) {

	        	// if there is an error, stop everything and return that
	        	// ie an error connecting to the database
	            if (err)
	                return done(err);

				// if the user is found, then log them in
	            if (user) 
	            {
	                return done(null, user); // user found, return that user
	            } 
	            else 
	            {
	                // if there is no user found with that facebook id, check for email id
	                if (profile.emails){if(profile.emails[0].value!="undefined") 
	                {
	                	User.findOne({'email':profile.emails[0].value},function(err,user){
	                		if (err) return done(err);
	                		if (user)
	                			{ 
	                				User.findOneAndUpdate({'_id':user._id},{'facebook.id':profile.id},function(err,user){
	                					if(!err) return done(null,user);
	                				});
	                			}
	                		else
	                		{
	                			 var newUser            = new User();

								// set all of the facebook information in our user model
				                newUser.facebook.id    = profile.id; // set the users facebook id	                
				               // newUser.facebook.token = token; // we will save the token that facebook provides to the user	                
				                newUser.fullName  = profile.displayName; // look at the passport user profile to see how names are returned
				                newUser.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
				                newUser.password="fb";
				                newUser.rating=5;
								// save our user to the database
				                newUser.save(function(err) {
				                    if (err)
				                        throw err;

				                    // if successful, return the new user
				                    return done(null, newUser);
				            	});
				            }
				        });
	                }}
	                else
	                {
		                var newUser            = new User();
						// set all of the facebook information in our user model
		                newUser.facebook.id    = profile.id; // set the users facebook id	                
		               // newUser.facebook.token = token; // we will save the token that facebook provides to the user	                
		                newUser.fullName  = profile.displayName; // look at the passport user profile to see how names are returned
		                newUser.password="fb";
						// save our user to the database
						newUser.rating=5;
		                newUser.save(function(err) {
		                    if (err)
		                        throw err;

		                    // if successful, return the new user
		                    return done(null, newUser);
		            	});
	            	}
	            }
	        });
        });

    }));

	passport.use('github',new GitHubStrategy({
	     clientID        : configAuth.githubAuth.clientID,
        clientSecret    : configAuth.githubAuth.clientSecret,
        callbackURL     : configAuth.githubAuth.callbackURL
 	 	},
	 	 function(accessToken, refreshToken, profile, done) {
	    // asynchronous verification, for effect...
	    	process.nextTick(function () {
	    		console.log(profile);
	    		User.findOne({ 'github.id' : profile.id }, function(err, user) {

	        	// if there is an error, stop everything and return that
	        	// ie an error connecting to the database
	            if (err)
	                return done(err);

				// if the user is found, then log them in
	            if (user) 
	            {
	                return done(null, user); // user found, return that user
	            } 
	            else 
	            {
	                // if there is no user found with that github id, create them
	                if (profile.emails[0].value&&profile.emails[0].value!="undefined") 
	                {
	                	User.findOne({'email':profile.emails[0].value},function(err,user){
	                		if (err) return done(err);
	                		if (user)
	                			{ 
	                				User.findOneAndUpdate({'_id':user._id},{'github.id':profile.id},function(err,user){
	                					if(!err) return done(null,user);
	                				});
	                			}
	                		else
	                		{
	                			 var newUser            = new User();

								// set all of the github information in our user model
				                newUser.github.id    = profile.id; // set the users github id	                
				               // newUser.facebook.token = token; // we will save the token that facebook provides to the user	                
				                newUser.fullName  = profile.displayName; // look at the passport user profile to see how names are returned
				                newUser.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
				                newUser.password="fb";
				                newUser.rating=5;
								// save our user to the database
				                newUser.save(function(err) {
				                    if (err)
				                        throw err;

				                    // if successful, return the new user
				                    return done(null, newUser);
				            	});
				            }
				        });
	                }
	                else
	                {
		                var newUser            = new User();
						// set all of the facebook information in our user model
		                newUser.github.id    = profile.id; // set the users facebook id	                
		               // newUser.facebook.token = token; // we will save the token that facebook provides to the user	                
		                newUser.fullName  = profile.displayName; // look at the passport user profile to see how names are returned
		                newUser.password="fb";
		                newUser.rating=5;
						// save our user to the database
		                newUser.save(function(err) {
		                    if (err)
		                        throw err;

		                    // if successful, return the new user
		                    return done(null, newUser);
		            	});
	            	}
	            }

	        });
	    });
  	}));

		passport.use('google',new GoogleStrategy({
	     clientID        : configAuth.googleAuth.clientID,
        clientSecret    : configAuth.googleAuth.clientSecret,
        callbackURL     : configAuth.googleAuth.callbackURL
 	 	},
	 	 function(accessToken, refreshToken, profile, done) {
	    // asynchronous verification, for effect...
	    	process.nextTick(function () {
	    		
	    		User.findOne({ 'google.id' : profile.id }, function(err, user) {

	        	// if there is an error, stop everything and return that
	        	// ie an error connecting to the database
	            if (err)
	                return done(err);

				// if the user is found, then log them in
	            if (user) 
	            {
	                return done(null, user); // user found, return that user
	            } 
	            else 
	            {
	                // if there is no user found with that google id, create them
	                if (profile.emails[0].value&&profile.emails[0].value!="undefined") 
	                {
	                	User.findOne({'email':profile.emails[0].value},function(err,user){
	                		if (err) return done(err);
	                		if (user)
	                			{ 
	                				
	                				User.findOneAndUpdate({'_id':user._id},{'google.id':profile.id},function(err,user){
	                					if(!err) return done(null,user);
	                				});
	                			}
	                		else
	                		{
	                			 var newUser            = new User();

								// set all of the facebook information in our user model
				                newUser.google.id    = profile.id; // set the users facebook id	                
				               // newUser.facebook.token = token; // we will save the token that facebook provides to the user	                
				                newUser.fullName  = profile.displayName; // look at the passport user profile to see how names are returned
				                newUser.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
				                newUser.password="google";
				                newUser.rating=5;
								// save our user to the database
				                newUser.save(function(err) {
				                    if (err)
				                        throw err;

				                    // if successful, return the new user
				                    return done(null, newUser);
				            	});
				            }
				        });
	                }
	                else
	                {
		                var newUser            = new User();
						// set all of the facebook information in our user model
		                newUser.google.id    = profile.id; // set the users facebook id	                
		               // newUser.facebook.token = token; // we will save the token that facebook provides to the user	                
		                newUser.fullName  = profile.displayName; // look at the passport user profile to see how names are returned
		                newUser.password="google";
		                newUser.rating=5;
						// save our user to the database
		                newUser.save(function(err) {
		                    if (err)
		                        throw err;

		                    // if successful, return the new user
		                    return done(null, newUser);
		            	});
	            	}
	            }

	        });
	    });
  	}));
};

