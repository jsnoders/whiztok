var User = require('../models/users');
var mail =require('../controllers/mail_controller');

var response = {};
var flag = 0;

function register(req,res){
	console.log("Registering the user"+req.body.registerEmail);
	var email = req.body.registerEmail;
	var url=req.protocol + '://' + req.get('host') + req.originalUrl;

	User.findOne({email : email}, function(err,doc){
	if(err){
			var response = {
				error: "DB error"
			};
			res.json(500,response);
			return;
	}
	else if(!doc){
			console.log("New User ,Sending mail");
			mail.sendMail(email,url,req,res);
			var response={email:"String"};
			response.email= email;
			res.json(200,response);
			return;			
		}
		else{
			var response = {
				error: "This email is already taken."
			};
			res.json(200,response);
			return;
		}
	});
	
}

exports.register=register;