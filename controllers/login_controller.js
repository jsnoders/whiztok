var User = require('../models/users.js');
var email = require('emailjs');
var generatePassword = require('password-generator');
var server = email.server.connect({
	user : "whiztok@gmail.com",
	password : "whiztok123",
	host : "smtp.gmail.com",
	ssl : true
});


function isValidUserPassword(email, password, done) {
	console.log('In isValidUserPassword');
	User.findOne({email : email}, function(err, user) {
		if (err) {
			return done(err);
		}

		if (!user) {
			return done(null, false, {message : 'Email provided is not registered.Plese retry.'});
		}

		if (password === user.password) {
			return done(null, user);
		}

		return done(null, false, {message : 'Password provided is incorrect. Plese retry.'});
	});
};

function forgotpwd(req,res){
	var email = req.body.forgetemail;
	console.log(email);
	var pwd=generatePassword();
	console.log("Yahan aaya");
	User.findOneAndUpdate({email : email}, {password:pwd},function(err,doc){
		if(err){
			console.log("Error in sendmail:" +err);
			var response = {
				error: "DB error"
			};
			res.json(500,response);
			return;
		}
		if(doc){
			var emailText = "Your password has been reset to";
			emailText = emailText +" \n\n"+pwd+"\n\n You can reset it from your account. Thank you !!";
			console.log(emailText);
			server.send({
				text : emailText,
				from : "whiztok@gmail.com",
				to   : email,
				subject : "Password recovery-Whiztok"
			}, function(err, message) {
				if (err) {
					console.log(err);
				} 
				else {
						
					}
			});
			var response = {
							data: "New password is sent to entered email .Please check your email for login"
						};
						res.json(200,response);
		}
		else{
			
			var response = {
				error: "This email is not registered."
			};
			res.json(200,response);
		}
	});
}

exports.isValidUserPassword = isValidUserPassword;
exports.forgotpwd=forgotpwd;
