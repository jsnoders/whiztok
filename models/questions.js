var mongoose = require ('mongoose');

Questions = mongoose.Schema({
	title 		: String,
	description : String,
	postDate 	: String,
	status 		: {type : Boolean,default:0},
	askedBy 	: {name:String,id:String},
	answeredBy : [{
			ansId : String,
			ansDate : Date,
			vote : Boolean
	}],
	tags:[],
	polledUser:[{}],
	answered : {type : Boolean,default:false}

});

	var Question = mongoose.model("Question", Questions);
	module.exports = Question;