var mongoose = require ('mongoose');

UserSchema = mongoose.Schema({
	fullName : String,
	email : String,
	password : String,
	rating : {type : Number , default : 1},
	facebook:{id:Number},
	github:{id:Number},
	google:{id:Number},
	skills : [],
	questions:[],
	badges:[],

	notification:[],
	

	});

	var User = mongoose.model("User", UserSchema);
	module.exports = User;
